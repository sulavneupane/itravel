package controller.servlets;

import database.Database;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet(name = "CheckNewPosts")
public class CheckNewPosts extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        User user = (User) request.getSession().getAttribute("loggedInUser");

        if (user == null) {
            response.sendRedirect("/index.jsp");
            return;
        }

        Timestamp lastTimestamp = (Timestamp) request.getSession().getAttribute("lastTimestamp");

        if (lastTimestamp == null) {
            lastTimestamp = new Timestamp(System.currentTimeMillis());
        }

        Connection con = Database.getConnection(request, response);

        response.setContentType("text/plain");

        try {
            String sql2 = "SELECT COUNT(*) FROM posts WHERE datetime>?";
            PreparedStatement ps2 = con.prepareStatement(sql2);
            ps2.setTimestamp(1, lastTimestamp);
            ResultSet rs2 = ps2.executeQuery();
            if (rs2.next()) {
                if (rs2.getInt(1) > 0)
                    out.print("true");
                else
                    out.print("false");

            } else {
                out.print("false");

            }
        } catch (SQLException e) {
            e.printStackTrace();
            out.print("false");

        }
        out.flush();
        out.close();

        request.getSession().setAttribute("lastTimestamp", new Timestamp(System.currentTimeMillis()));

    }
}
