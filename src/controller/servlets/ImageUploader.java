package controller.servlets;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.RandomStringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "ImageUploader")
public class ImageUploader extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            ServletFileUpload sf = new ServletFileUpload(new DiskFileItemFactory());
            List<FileItem> multiFiles = sf.parseRequest(request);

            List<String> files = new ArrayList<>();

            String postImagesLocation = request.getServletContext().getInitParameter("imagesLocation");
            String filePath = getServletContext().getRealPath("/") + postImagesLocation;

            File path = new File(filePath);
            if (!path.exists()) {
                boolean status = path.mkdirs();
            }

            for (FileItem item : multiFiles) {
                String[] extensions = item.getName().split("\\.");
                String fileName = String.format("%s.%s", RandomStringUtils.randomAlphanumeric(15), extensions[extensions.length - 1]);
                item.write(new File(filePath + fileName));
                files.add(postImagesLocation + fileName);
            }

            String filesString = String.join(",", files);

            response.setContentType("application/json");
            response.getWriter().write(filesString);

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

}
